---
title: About
layout: page
---
Hi! Im Erik
I am currently studying computer science and in my spare time I dedicate myself to Ethical Hacking.

I want to focus on pentesting and Red Team


Social Media:

- <a href="https://github.com/erik-451">Github</a>                                                                                            

- <a href="https://twitter.com/eriik451">Twitter</a>

- <a href="https://www.hackthebox.eu/profile/404201">HackTheBox</a>

- <a href="https://tryhackme.com/p/Erik451">TryHackMe</a>
